from django.shortcuts import render
from datetime import datetime, date
# Enter your name here
mhs_name = 'Sayid Abyan Rizal Shiddiq' # TODO Implement this
curr_year = int(datetime.now().strftime("%Y"))
birth_date = date(1999, 7, 21) #TODO Implement this, format (Year, Month, Date)
npm = 1706022445 # TODO Implement this
univ = 'University of Indonesia'
sikiri = 'Ahmad Supriyanto'
sikirinpm = 1706075016
sikiri_date = date(1999, 3, 20)
sikanan = 'Farhan Azyumardhi Azmi'
sikanannpm = 1706979234
sikanan_date = date(1999, 8, 20)
# Create your views here.
def index(request):
    response = {'name': mhs_name, 'age': calculate_age(birth_date.year), 'npm': npm, 'univ': univ, 'sikiri':sikiri,'sikirinpm':sikirinpm, 'sikanan':sikanan, 'sikanannpm':sikanannpm, 'sikiri_date':calculate_age(sikiri_date.year), 'sikanan_date':calculate_age(sikanan_date.year)}
    return render(request, 'index_lab1.html', response)

def calculate_age(birth_year):
    return curr_year - birth_year if birth_year <= curr_year else 0
